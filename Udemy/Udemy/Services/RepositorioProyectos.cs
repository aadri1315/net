﻿using Udemy.Models;

namespace Udemy.Services
{
    public interface IRepositorioProyectos
    {
        List<Proyecto> ObtenerProyectos();
    }
    public class RepositorioProyectos: IRepositorioProyectos
    {
        public List<Proyecto> ObtenerProyectos()
        {
            return new List<Proyecto>() { new Proyecto
            {
                Titulo = "Waitless",
                Descripcion = "Full stack en Java.",
                ImagenURL = "https://www.waitless24.com/public/favicon-60.png",
                Link = "https://www.waitless24.com/ESP/index.html"
            }

            };
        }
    }
}
