﻿namespace Udemy.Models
{
    public class HomeIndexViewModel
    {
        public IEnumerable<Proyecto> Proyectos { get; set; }
    }
}
