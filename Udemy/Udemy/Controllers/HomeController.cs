﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Udemy.Models;
using Udemy.Services;

namespace Udemy.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepositorioProyectos repositorioProyectos;
        private readonly IServicioEmail servicioEmail;

        public HomeController(IRepositorioProyectos repositorioProyectos,
            IServicioEmail servicioEmail)
        {
            this.repositorioProyectos = repositorioProyectos;
            this.servicioEmail = servicioEmail;
        }

        public IActionResult Index()
        {
            //var persona = new Persona()
            //{
            //    /*Name = "Adrián García Remesal",
            //    Edad = 21*/
            //};

            ////Pasando datos con ViewBag solo al index de Home
            ////ViewBag.Nombre = "Adrián García Remesal";
            ////Pasando datos fuertemente tipados
            //return View(/*persona*/);

            //Esto es por si quiero enviar una cantidad definida con el .Take()
            var proyectos = repositorioProyectos.ObtenerProyectos().ToList();
            var modelo = new HomeIndexViewModel() { Proyectos = proyectos };   
            return View(modelo);
        }

        public IActionResult Proyectos()
        {
            //Esto es por si quiero coger todo los proyectos
            var proyectos = repositorioProyectos.ObtenerProyectos();

            return View(proyectos);
        }

        public IActionResult Contacto()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Contacto(ContactoViewModel contactoViewModel)
        {
            await servicioEmail.Enviar(contactoViewModel);
            return RedirectToAction("Gracias");
        }

        public IActionResult Gracias()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}